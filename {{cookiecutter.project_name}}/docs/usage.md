# Usage

## Getting help

To get information for the `{{ cookiecutter.project_name }}` command, use the ``--help`` flag:

{{ "{{" }} cli("{{ cookiecutter.project_name }}", "--help") {{ "}}" }}

