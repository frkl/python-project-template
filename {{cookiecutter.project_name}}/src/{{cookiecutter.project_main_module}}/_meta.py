# -*- coding: utf-8 -*-
from typing import Any, Dict

project_name = "{{ cookiecutter.project_name }}"
app_name = "{{ cookiecutter.project_name }}"
project_main_module = "{{ cookiecutter.project_main_module }}"
project_slug = "{{ cookiecutter.project_slug }}"

pyinstaller: Dict[str, Any] = {}
