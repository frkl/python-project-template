# -*- coding: utf-8 -*-

import io
import logging
import os
from typing import Optional


from pkg_resources import DistributionNotFound, get_distribution

log = logging.getLogger("frkl")

"""Top-level package for {{ cookiecutter.project_name }}."""

__author__ = """{{ cookiecutter.full_name }}"""
__email__ = '{{ cookiecutter.email }}'

try:
    # Change here if project is renamed and does not equal the package name
    dist_name = __name__
    __version__ = get_distribution(dist_name).version
except DistributionNotFound:

    try:
        version_file = os.path.join(os.path.dirname(__file__), "version.txt")

        if os.path.exists(version_file):
            with io.open(version_file, encoding="utf-8") as vf:
                __version__ = vf.read()
        else:
            __version__ = "unknown"

    except (Exception):
        pass

    if __version__ is None:
        __version__ = "unknown"

finally:
    del get_distribution, DistributionNotFound


try:
    from frkl.project_meta.app_environment import AppEnvironment

    {{ cookiecutter.project_slug | upper }}: AppEnvironment = AppEnvironment(main_module="{{ cookiecutter.project_main_module }}")
except Exception as e:
    log.debug(f"Can't create AppEnvironment (probably pkg 'frkl.project_meta' not in virtualenv): {e}")
    FRKL_TESTAPP = None
