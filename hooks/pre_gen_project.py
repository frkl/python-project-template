import re
import sys


SLUG_REGEX = r'^[_a-zA-Z][_a-zA-Z0-9]+$'
slug_name = '{{ cookiecutter.project_slug }}'

if not re.match(SLUG_REGEX, slug_name):
    print(f"ERROR: The project slug ({slug_name}) is not a valid Python module name. Please do not use a - and use _ instead")
    sys.exit(1)


MODULE_REGEX = r'^[_a-zA-Z][_a-zA-Z0-9\.]+$'
module_name = '{{ cookiecutter.project_main_module }}'

if not re.match(MODULE_REGEX, module_name):
    print(f"ERROR: The project main module ({module_name}) is not a valid Python module name. Please do not use a - and use _ instead.")

    #Exit to cancel project
    sys.exit(1)
