# -*- coding: utf-8 -*-

"""Main module."""

import logging

log = logging.getLogger("{{ cookiecutter.project_slug }}")
